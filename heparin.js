app = {};

// window.onload = function () {
//     var savedPW = document.getElementById('savedPW').textContent;
//     if (savedPW == 'hsj') {
//       hide.style.display = 'block';
//       pw.style.display = 'none';
//     } 
//   }

//   app = {};
//   app.submitPW = function() {
//       var submittedPW = document.getElementById('enteredPW').value;
//       var emptyPW = document.getElementById('enteredPW');
//       var savedPW2 = document.getElementById('savedPW');
//       savedPW2.innerHTML = submittedPW;
//       localStorage.setItem('savedPW2', savedPW2.innerHTML);

//       var savedPW = document.getElementById('savedPW').textContent;
//       if (savedPW == 'hsj') {
//         hide.style.display = 'block';
//         pw.style.display = 'none';
//         $('#disclaimer').modal('show');
//       }  else {
//         emptyPW.value = '';
//         incorrect2 = '<small> Incorrect. Please get passkey from HSJ Pharmacy.</small>';
//         }
//     incorrect.innerHTML = incorrect2;
//       } 

// var savedPW2 = localStorage.getItem('savedPW2');
// savedPW.innerHTML = savedPW2;




app.Calc1 = function() {
    var kg1 = document.getElementById('weight1').value ;
    var ACSBol = kg1 * 60  ;
    var NVS = kg1 * 70 ;
    var PE = kg1 * 80 ;
    
    var ACSBol = Math.min(ACSBol , 5000);
    var NVS = Math.min(NVS , 5000);
    var PE = Math.min(PE , 8000);
    ptnKg.innerHTML = 'Current weight: ' + kg1 + ' kg';
    Ans1.innerHTML = '<b>'+  ACSBol + ' units</b>' + ' (fibrinolytic: max 4000 units) ' + '(w/o fibrinolytic: max 5000 units)';
    Ans2.innerHTML = '<b>'+ NVS + ' units</b>';
    Ans3.innerHTML = '<b>'+ PE + ' units</b>';

    if (document.querySelector(".input1").value === "") {
        ptnKg.innerHTML = '';
        Ans1.innerHTML = '';
        Ans2.innerHTML = '';
        Ans3.innerHTML = '';
      }
};

app.Calc2 = function() {
    var kg1 = document.getElementById('weight1').value ;
    var ACS = kg1 * 12  ;
    var NVS = kg1 * 15 ;
    var PE = kg1 * 18 ;
    
    var ACS = Math.min(ACS , 1000);
    var NVS = Math.min(NVS , 1000);
    var PE = Math.min(PE , 1500);
    ptnKg2.innerHTML = 'Current weight: ' + kg1 + ' kg';
    Ans4.innerHTML = '<b>'+  ACS + ' units/hr</b>';
    Ans5.innerHTML = '<b>'+ NVS + ' units/hr</b>';
    Ans6.innerHTML = '<b>'+ PE + ' units/hr</b>';

    if (document.querySelector(".input1").value === ""){
        ptnKg2.innerHTML = '';
        Ans4.innerHTML = '';
        Ans5.innerHTML = '';
        Ans6.innerHTML = '';
      }
};


app.Calc3 = function() {

    
    var curRate = document.getElementById('rate1').value 
    var curRate2 = curRate / 500 ; 

    currRate.innerHTML = 'Current rate: ' + curRate + ' units/hour';
    Ans1.innerHTML = 'Infusion Rate = <b>'+  curRate2 + ' ml/hour</b>';

    if (document.querySelector(".input1").value === "") {
        Ans1.innerHTML = '';
        currRate.innerHTML = '';
        }
} ;


app.Calc4 = function() {
    var APTT = document.getElementById('APTT').value ;
    var kg1 = document.getElementById('weight1').value;
    if (APTT < 50) {
        bolus1 = 60 * kg1;
    } else {
        bolus1 = 0;
    }

    if (APTT >= 101 && APTT <= 110) {
        hold = 30;
    } else if (APTT > 110){
        hold = 60;
    } else {
        hold = 0;
    }

    if (APTT < 50) {
        rateC = '<b>+ </b>' + 3 * kg1;
    } else if (APTT >= 50 && APTT <= 64){
        rateC = '<b>+ </b>' + 2 * kg1;
    } else if (APTT >= 65 && APTT <= 90){
        rateC = 0;
    }  else if (APTT >= 91 && APTT <= 100){
        rateC = '<b>- </b>' + 1 * kg1;
    }  else if (APTT >= 101 && APTT <= 110){
        rateC = '<b>- </b>' + 2 * kg1;
    }  else {
        rateC = '<b>- </b>' + 3 * kg1;
    }

    if (APTT >= 65 && APTT <= 90) {
        repeatAPTT = 'Next morning (within 24 hours)';
    }  else {
        repeatAPTT = '4-6 hours';
    }

    ptnKg.innerHTML = 'Current weight: ' + kg1 + ' kg';
    curAPTT.innerHTML = 'Current APTT: ' + APTT + ' seconds';
    Ans1.innerHTML = bolus1 ;
    Ans2.innerHTML = hold;
    Ans3.innerHTML = rateC;
    Ans4.innerHTML = repeatAPTT;

    if (document.querySelector(".input1").value === "" || document.querySelector(".input2").value === "" ) {
        ptnKg.innerHTML = '';
        curAPTT.innerHTML = '';
        Ans1.innerHTML = '';
        Ans2.innerHTML = '';
        Ans3.innerHTML = '';
        Ans4.innerHTML = '';
      }
}

app.Calc5 = function() {
    var APTT = document.getElementById('APTT').value ;
    var kg1 = document.getElementById('weight1').value;
    if (APTT < 50) {
        bolus1 = 80 * kg1;
    } else if (APTT >= 50 && APTT <= 64){
        bolus1 = 40 * kg1;
    } else {
        bolus1 = 0;
    }

    if (APTT > 110) {
        hold = 60;
    } else {
        hold = 0;
    }

    if (APTT < 50) {
        rateC = '<b>+ </b>' + 4 * kg1;
    } else if (APTT >= 50 && APTT <= 64){
        rateC = '<b>+ </b>' + 2 * kg1;
    } else if (APTT >= 65 && APTT <= 100){
        rateC = 0;
    }  else if (APTT >= 101 && APTT <= 110){
        rateC = '<b>- </b>' + 2 * kg1;
    }  else {
        rateC = '<b>- </b>' + 3 * kg1;
    }

    if (APTT >= 65 && APTT <= 100) {
        repeatAPTT = 'Next morning (within 24 hours)';
    }  else {
        repeatAPTT = '4-6 hours';
    }

    ptnKg.innerHTML = 'Current weight: ' + kg1 + ' kg';
    curAPTT.innerHTML = 'Current APTT: ' + APTT + ' seconds';
    Ans1.innerHTML = bolus1 ;
    Ans2.innerHTML = hold;
    Ans3.innerHTML = rateC;
    Ans4.innerHTML = repeatAPTT;

    if (document.querySelector(".input1").value === "" || document.querySelector(".input2").value === "" ) {
        ptnKg.innerHTML = '';
        curAPTT.innerHTML = '';
        Ans1.innerHTML = '';
        Ans2.innerHTML = '';
        Ans3.innerHTML = '';
        Ans4.innerHTML = '';
      }
}

function toggleSpan() {
    var mySpan1 = document.getElementById('span1');
    var span1Display = mySpan1.style.display;

    if (span1Display == 'none') {
      mySpan1.style.display = 'block';
    } else if (span1Display == 'block') {
      mySpan1.style.display = 'none';
    }

}

function toggleSpan1() {
    var mySpan1 = document.getElementById('span1');
    var span1Display = mySpan1.style.display;
    var mySpan2 = document.getElementById('span2');
    var span2Display = mySpan2.style.display;

    if (span1Display == 'none') {
      mySpan1.style.display = 'block';
    } else if (span1Display == 'block') {
      mySpan1.style.display = 'none';
    }

    if (span2Display == 'block') {
        mySpan2.style.display = 'none';
    }

}

function toggleSpan2() {
    var mySpan1 = document.getElementById('span1');
    var span1Display = mySpan1.style.display;
    var mySpan2 = document.getElementById('span2');
    var span2Display = mySpan2.style.display;

    if (span2Display == 'none') {
      mySpan2.style.display = 'block';
    } else if (span2Display == 'block') {
      mySpan2.style.display = 'none';
    }

    if (span1Display == 'block') {
        mySpan1.style.display = 'none';
    }

}

function refreshPage(){
    window.location.reload();
  }