app = {};

// window.onload = function () {
//   var savedPW = document.getElementById('savedPW').textContent;
//   if (savedPW == 'hsj') {
//     hide.style.display = 'block';
//     pw.style.display = 'none';
//   } 
// }

// app = {};
// app.submitPW = function() {
//     var submittedPW = document.getElementById('enteredPW').value;
//     var emptyPW = document.getElementById('enteredPW');
//     var savedPW2 = document.getElementById('savedPW');
//     savedPW2.innerHTML = submittedPW;
//     localStorage.setItem('savedPW2', savedPW2.innerHTML);

//     var savedPW = document.getElementById('savedPW').textContent;
//     if (savedPW == 'hsj') {
//       hide.style.display = 'block';
//       pw.style.display = 'none';
//       $('#disclaimer').modal('show');
//     }  else {
//       emptyPW.value = '';
//       incorrect2 = '<small> Incorrect. Please get passkey from HSJ Pharmacy.</small>';
//       }
//   incorrect.innerHTML = incorrect2;
//     } 

// var savedPW2 = localStorage.getItem('savedPW2');
// savedPW.innerHTML = savedPW2;



app.AdreCal = function() {
  var ptnKg = document.getElementById('adreKg').value;
  var adreDose = document.getElementById('adreDose').value;
  var adreAnsSingleDil = adreDose * ptnKg; 
  var adreAnsDouDil = adreDose * ptnKg * 0.5;
  var adreAnsQuadDil = adreDose * ptnKg * 0.25;

    adreKg2.innerHTML = 'Current weight: ' + ptnKg + ' kg';
    adreDose2.innerHTML = 'Current dose: ' + adreDose + ' mcg/kg/min';
    adreAns1.innerHTML = '<b>Single Strength Dilution: </b> '+  adreAnsSingleDil + ' ml/hour';
    adreAns2.innerHTML = '<b>Double Strength Dilution: </b> '+  adreAnsDouDil + ' ml/hour';
    adreAns3.innerHTML = '<b>Quad Strength Dilution: </b> '+  adreAnsQuadDil + ' ml/hour';

    if (document.querySelector(".input1").value === "" || document.querySelector(".input2").value === "" ) {
      adreKg2.innerHTML = '';
      adreDose2.innerHTML = '';
      adreAns1.innerHTML = '';
      adreAns2.innerHTML = '';
      adreAns3.innerHTML = '';
    }
};

app.AdreCal2 = function() {
  var ptnKg = document.getElementById('adreKg3').value;
  var currRate = document.getElementById('currRate').value;
  var adreAnsSingleDil = currRate / ptnKg; 
  var adreAnsDouDil = currRate / ptnKg / 0.5;
  var adreAnsQuadDil = currRate / ptnKg / 0.25;

    adreKg4.innerHTML = 'Current weight: ' + ptnKg + ' kg';
    currRate2.innerHTML = 'Current rate: ' + currRate + ' ml/hour';
    doseAns1.innerHTML = '<b>Single Strength Dilution: </b> '+  adreAnsSingleDil.toFixed(2)  + ' mcg/kg/min';
    doseAns2.innerHTML = '<b>Double Strength Dilution: </b> '+  adreAnsDouDil.toFixed(2)  + ' mcg/kg/min';
    doseAns3.innerHTML = '<b>Quad Strength Dilution: </b> '+  adreAnsQuadDil.toFixed(2)  + ' mcg/kg/min';

    if (document.querySelector(".input3").value === "" || document.querySelector(".input4").value === "" ) {
      adreKg4.innerHTML = '';
      currRate2.innerHTML = '';
      doseAns1.innerHTML = '';
      doseAns2.innerHTML = '';
      doseAns3.innerHTML = '';
    }
};


app.noradreCal = function() {
    var ptnKg = document.getElementById('noradreKg').value;
    var noradreDose = document.getElementById('noradreDose').value;
    var infRate1 = noradreDose * ptnKg * 0.75;
    var infRate2 = noradreDose * ptnKg * 0.375;
    var infRate3 = noradreDose * ptnKg * 0.1875;


    noradreKg2.innerHTML = 'Current weight: ' + ptnKg + ' kg';
    noradreDose2.innerHTML = 'Current dose: ' + noradreDose + ' mcg/kg/min';
    noradreAns1.innerHTML = '<b>Single Strength Dilution: </b> '+  infRate1.toFixed(2) + ' ml/hour';
    noradreAns2.innerHTML = '<b>Double Strength Dilution: </b> '+  infRate2.toFixed(2) + ' ml/hour';
    noradreAns3.innerHTML = '<b>Quad Strength Dilution: </b> '+  infRate3.toFixed(2) + ' ml/hour';

    if (document.querySelector(".input1").value === "" || document.querySelector(".input2").value === "" ) {
      noradreKg2.innerHTML = '';
      noradreDose2.innerHTML = '';
      noradreAns1.innerHTML = '';
      noradreAns2.innerHTML = '';
      noradreAns3.innerHTML = '';
    }
};

app.noradreCal2 = function() {
  var ptnKg = document.getElementById('noradreKg3').value;
  var currRate = document.getElementById('currRate').value;
  var dose1 = currRate / ptnKg / 0.75;
  var dose2 = currRate / ptnKg / 0.375;
  var dose3 = currRate / ptnKg / 0.1875;


  noradreKg4.innerHTML = 'Current weight: ' + ptnKg + ' kg';
  currRate2.innerHTML = 'Current rate: ' + currRate + ' ml/hour';
  doseAns1.innerHTML = '<b>Single Strength Dilution: </b> '+  dose1.toFixed(2) + ' mcg/kg/min';
  doseAns2.innerHTML = '<b>Double Strength Dilution: </b> '+  dose2.toFixed(2) + ' mcg/kg/min';
  doseAns3.innerHTML = '<b>Quad Strength Dilution: </b> '+  dose3.toFixed(2) + ' mcg/kg/min';

  if (document.querySelector(".input3").value === "" || document.querySelector(".input4").value === "" ) {
    noradreKg4.innerHTML = '';
    currRate2.innerHTML = '';
    doseAns1.innerHTML = '';
    doseAns2.innerHTML = '';
    doseAns3.innerHTML = '';
  }
};

app.dopaCal = function() {
    var ptnKg = document.getElementById('dopaKg').value;
    var dopaDose = document.getElementById('dopaDose').value;
    var infRateSingle = dopaDose * ptnKg * 0.015;
    var infRateDouble = dopaDose * ptnKg * 0.0075;

    dopaKg2.innerHTML = 'Current weight: ' + ptnKg + ' kg';
    dopaDose2.innerHTML = 'Current dose: ' + dopaDose + ' mcg/kg/min';
    dopaAns1.innerHTML = '<b>Single Strength Dilution: </b> '+ infRateSingle.toFixed(2) + ' ml/hour';
    dopaAns2.innerHTML = '<b>Double Strength Dilution: </b> '+ infRateDouble.toFixed(2) + ' ml/hour';
    
    if (document.querySelector(".input1").value === "" || document.querySelector(".input2").value === "" ) {
      dopaKg2.innerHTML = '';
      dopaDose2.innerHTML = '';
      dopaAns1.innerHTML = '';
      dopaAns2.innerHTML = '';
    }
};

app.dopaCal2 = function() {
  var ptnKg = document.getElementById('dopaKg3').value;
  var currRate = document.getElementById('currRate').value;
  var doseSingle = currRate / ptnKg / 0.015;
  var doseDouble = currRate / ptnKg / 0.0075;

  dopaKg4.innerHTML = 'Current weight: ' + ptnKg + ' kg';
  currRate2.innerHTML = 'Current rate: ' + currRate + ' ml/hour';
  doseAns1.innerHTML = '<b>Single Strength Dilution: </b> '+ doseSingle.toFixed(2) + ' mcg/kg/min';
  doseAns2.innerHTML = '<b>Double Strength Dilution: </b> '+ doseDouble.toFixed(2) + ' mcg/kg/min';
  
  if (document.querySelector(".input3").value === "" || document.querySelector(".input4").value === "" ) {
    dopaKg4.innerHTML = '';
    currRate2.innerHTML = '';
    doseAns1.innerHTML = '';
    doseAns2.innerHTML = '';
  }
};

app.dobuCal = function() {
    var ptnKg = document.getElementById('dobuKg').value;
    var dobuDose = document.getElementById('dobuDose').value;
    var infRateSingle = dobuDose * ptnKg * 0.012;
    var infRateDouble = dobuDose * ptnKg * 0.006;

    dobuKg2.innerHTML = 'Current weight: ' + ptnKg + ' kg';
    dobuDose2.innerHTML = 'Current dose: ' + dobuDose + ' mcg/kg/min';
    dobuAns1.innerHTML = '<b>Single Strength Dilution: </b> '+ infRateSingle.toFixed(2) + ' ml/hour';
    dobuAns2.innerHTML = '<b>Double Strength Dilution: </b> '+ infRateDouble.toFixed(2) + ' ml/hour';
    
    if (document.querySelector(".input1").value === "" || document.querySelector(".input2").value === "" ) {
      dobuKg2.innerHTML = '';
      dobuDose2.innerHTML = '';
      dobuAns1.innerHTML = '';
      dobuAns2.innerHTML = '';
    }
};

app.dobuCal2 = function() {
  var ptnKg = document.getElementById('dobuKg3').value;
  var currRate = document.getElementById('currRate').value;
  var doseSingle = currRate / ptnKg / 0.012;
  var doseDouble = currRate / ptnKg / 0.006;

  dobuKg4.innerHTML = 'Current weight: ' + ptnKg + ' kg';
  currRate2.innerHTML = 'Current rate: ' + currRate + ' ml/hour';
  doseAns1.innerHTML = '<b>Single Strength Dilution: </b> '+ doseSingle.toFixed(2) + ' mcg/kg/min';
  doseAns2.innerHTML = '<b>Double Strength Dilution: </b> '+ doseDouble.toFixed(2) + ' mcg/kg/min';
  
  if (document.querySelector(".input3").value === "" || document.querySelector(".input4").value === "" ) {
    dobuKg4.innerHTML = '';
    currRate2.innerHTML = '';
    doseAns1.innerHTML = '';
    doseAns2.innerHTML = '';
  }
};

function toggleSpan1() {
    var mySpan1 = document.getElementById('span1');
    var span1Display = mySpan1.style.display;
    var mySpan2 = document.getElementById('span2');
    var span2Display = mySpan2.style.display;
    var mySpan3 = document.getElementById('span3');
    var span3Display = mySpan3.style.display;
    if (span1Display == 'none') {
      mySpan1.style.display = 'block';
    } else if (span1Display == 'block') {
      mySpan1.style.display = 'none';
    }

    if (span2Display == 'block') {
        mySpan2.style.display = 'none';
    }

    if (span3Display == 'block') {
        mySpan3.style.display = 'none';
    }

}

function toggleSpan2() {
    var mySpan1 = document.getElementById('span1');
    var span1Display = mySpan1.style.display;
    var mySpan2 = document.getElementById('span2');
    var span2Display = mySpan2.style.display;
    var mySpan3 = document.getElementById('span3');
    var span3Display = mySpan3.style.display;
    if (span2Display == 'none') {
      mySpan2.style.display = 'block';
    } else if (span2Display == 'block') {
      mySpan2.style.display = 'none';
    }

    if (span1Display == 'block') {
        mySpan1.style.display = 'none';
    }

    if (span3Display == 'block') {
        mySpan3.style.display = 'none';
    }

}

function toggleSpan3() {
    var mySpan1 = document.getElementById('span1');
    var span1Display = mySpan1.style.display;
    var mySpan2 = document.getElementById('span2');
    var span2Display = mySpan2.style.display;
    var mySpan3 = document.getElementById('span3');
    var span3Display = mySpan3.style.display;
    if (span3Display == 'none') {
      mySpan3.style.display = 'block';
    } else if (span3Display == 'block') {
      mySpan3.style.display = 'none';
    }

    if (span1Display == 'block') {
        mySpan1.style.display = 'none';
    }

    if (span2Display == 'block') {
        mySpan2.style.display = 'none';
    }

}

function toggleSpanIOS() {
    var mySpan1 = document.getElementById('IOSSpan');
    var span1Display = mySpan1.style.display;
    var mySpan2 = document.getElementById('androidSpan');
    var span2Display = mySpan2.style.display;
    if (span1Display == 'none') {
      mySpan1.style.display = 'block';
    } else if (span1Display == 'block') {
      mySpan1.style.display = 'none';
    }
    if (span2Display == 'block') {
        mySpan2.style.display = 'none';
    }
}

function toggleSpanAndroid() {
    var mySpan1 = document.getElementById('IOSSpan');
    var span1Display = mySpan1.style.display;
    var mySpan2 = document.getElementById('androidSpan');
    var span2Display = mySpan2.style.display;
    if (span2Display == 'none') {
      mySpan2.style.display = 'block';
    } else if (span2Display == 'block') {
      mySpan2.style.display = 'none';
    }
    if (span1Display == 'block') {
        mySpan1.style.display = 'none';
    }
}



function refreshPage(){
    window.location.reload();
  }
