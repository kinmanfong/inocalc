var staticCacheName = 'site-static-v14.1';
var assets = [
    '/',
    '/index.html',
    '/img/calculator.jpg',
    '/img/calculator144.png',
    'img/A1.jpg',
    'img/A2.jpg',
    'img/i1.jpg',
    'img/i2.jpg',
    'img/i3.jpg',
    'img/hep1.jpg',
    'img/hep2.jpg',
    'img/hep3.jpg',
    '/about.html',
    '/about',
    '/adrenaline.html',
    '/adrenaline',
    '/dopadobu.html',
    '/dopadobu',
    '/hepacs.html',
    '/hepacs',
    '/heparin.html',
    '/heparin',
    '/heparin2.html',
    '/heparin2',
    '/hepinf.html',
    '/hepinf',
    '/heppedvt.html',
    '/heppedvt',
    '/iradrenaline.html',
    '/iradrenaline',
    '/irdobu.html',
    '/irdobu',
    '/irdopa.html',
    '/irdopa',
    '/irnorad.html',
    '/irnorad',
    '/noradrenaline.html',
    '/noradrenaline',
    '/precedex.html',
    '/precedex',
    '/salbu.html',
    '/salbu',
    '/vaso.html',
    '/vaso',
    '/master.css',
    '/calc.js',
    '/heparin.js',
    '/IR.js',
    '/precedex.js',
    '/salbuCal.js',
    '/vasoCalc.js',
    '/load.js',
    'https://cdn.jsdelivr.net/npm/popper.js@1.16.1/dist/umd/popper.min.js',
    'https://code.jquery.com/jquery-3.5.1.slim.min.js',
    'https://stackpath.bootstrapcdn.com/bootstrap/4.5.1/js/bootstrap.min.js',
    'https://fonts.googleapis.com/css?family=Montserrat&display=swap',
    'https://fonts.gstatic.com/s/montserrat/v24/JTUHjIg1_i6t8kCHKm4532VJOt5-QNFgpCtr6Hw5aXp-obK4.woff2',
];

self.addEventListener('install', evt => {
    // console.log('service worker has been installed');
    evt.waitUntil(
        caches.open(staticCacheName).then(cache => {
        //console.log('caching shell assets');
        cache.addAll(assets);
    })
    );
});

self.addEventListener('activate', evt => {
    //console.log('service worker has been activated');
    evt.waitUntil(
        caches.keys().then(keys =>{
            //console.log(keys); 
            return Promise.all(keys
                .filter(key => key !== staticCacheName)
                .map(key => caches.delete(key))
                )
        })
    );
});

self.addEventListener('fetch', evt => {
    //console.log('fetch event', evt);
    evt.respondWith(
        caches.match(evt.request).then(cacheRes => {
            return cacheRes || fetch(evt.request);
        })
    );
});

